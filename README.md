# BaiduFaceRecognitionDemo

#### 介绍
百度人脸在线识别demo

###### 在项目的```build.gralde```里面添加
```
allprojects {
		repositories {
			...
			maven { url 'https://www.jitpack.io' }
		}
	}

```
###### 在moulde的```build.gralde```里面添加

```
dependencies {
	        implementation 'com.gitee.cynthia_guo:BaiduFaceRecognitionDemo:1.0.0'
	}
```

#### 使用说明

1.  在你的application中初始化(注意，license_id 是你在百度后台管理申请的，文件也要放在项目的asset下面)
```
FaceSDKUtil.init(this, Information.LICENSE_ID, Information.LICENSE_FILE_NAME);
```
2.  使用圆形预览
```
//TODO 初始化
 @Override
    protected void onCreate(Bundle savedInstanceState) {
         FaceRoundSDKUtil.initFaceDetect(context, textureView,
                                    ScreenUtil.getRealWidth(), ScreenUtil.getRealWidth(), 0, 0);
        setCallback();
}
//TODO 监听到人脸（只支持单人）
private void setCallback(){
 FaceRoundSDKUtil.setFaceRecognitionCallback(new FaceRecognitionCallback() {
            @Override
            public void onFace(final Bitmap info) {
                //进行人脸识别
                 
            }

            @Override
            public void onPress(final String tip) {
                //提示用户 ，tip 可能为上下左右移动等
            }
        });
}

    @Override
    protected void onResume() {
        super.onResume();
        FaceRoundSDKUtil.resume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        FaceRoundSDKUtil.stop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        FaceRoundSDKUtil.setFaceRecognitionCallback(null);
        FaceRoundSDKUtil.destroy();
    }


```
3.  使用方形预览
```
   FaceSDKUtil.initFaceDetect(context, previewView, textureView, 2,
                                    ScreenUtil.getRealWidth(), ScreenUtil.getRealWidth());

```

#### 具体使用方法可参考:
[FaceRecognitionActivity.java](https://gitee.com/cynthia_guo/BaiduFaceRecognitionDemo/blob/master/demo/src/main/java/cn/cynthia/facedemo/FaceRecognitionActivity.java) 和 [FaceAddActivity.java](https://gitee.com/cynthia_guo/BaiduFaceRecognitionDemo/blob/master/demo/src/main/java/cn/cynthia/facedemo/FaceAddActivity.java)