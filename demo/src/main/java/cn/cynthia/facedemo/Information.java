package cn.cynthia.facedemo;

/**
 * Created by guoshuifang on 2020/1/4.
 * TODO
 */
public interface Information {
    /**
     * 百度的人脸识别这些东西需要实名认证后下载license.face-android文件
     */
    String BAIDU_APP_ID = "";//app id
    String BAIDU_API_KEY = "";//app key
    String BAIDU_SECRET_KEY = "";//secret_key
    String LICENSE_ID = "";//百度app的licenseId
    String LICENSE_FILE_NAME = "";//
}
