package cn.cynthia.facedemo.base;

import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import cn.cynthia.facedemo.base.listener.MySimpleRxCallBack;
import cn.dlc.commonlibrary.ui.base.BaseCommonActivity;
import cn.dlc.commonlibrary.utils.BindEventBus;

public abstract class BaseActivity extends BaseCommonActivity {
   /* @Override
    public Resources getResources() {
        Resources resources = super.getResources();
        return toModifyResource(resources);
    }

    protected Resources toModifyResource(Resources originalResources) {
        // 建议先在Application里面初始化  AdaptScreenUtils.init(context);
        // 假如设计图短边为1080像素
        return AdaptScreenEx.adaptShorter(originalResources, 1080);
    }*/

    @Override
    protected void onResume() {
        hideSystemUI();
        super.onResume();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if (hasFocus) {
            hideSystemUI();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * 隐藏状态栏和导航栏
     */
    protected void hideSystemUI() {

        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
            // Set the content to appear under the system bars so that the
            // content doesn't resize when the system bars hide and show.
            | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            // Hide the nav bar and status bar
            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    //沉浸式状态栏
    protected void hideStatusBar() {
        //沉浸式状态栏
        Window window = getWindow();
        View decorView = window.getDecorView();
        int option = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
        decorView.setSystemUiVisibility(option);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    protected class SimpleRxDataCallBack<T> extends MySimpleRxCallBack<T> {
        private String msg;
        private boolean cancelable;

        public SimpleRxDataCallBack(int msg, boolean cancelable) {
            this.msg = getString(msg);
            this.cancelable = cancelable;
            showWaitingDialog(msg, cancelable);
        }

        public SimpleRxDataCallBack(String msg, boolean cancelable) {
            this.msg = msg;
            this.cancelable = cancelable;
            showWaitingDialog(msg, cancelable);
        }

        public SimpleRxDataCallBack() {
        }

        @Override
        public void onSuccess(T t) {
            super.onSuccess(t);
            dismissWaitingDialog();
        }

        @Override
        public void onFailure(String message, Throwable tr) {
            super.onFailure(message, tr);
            dismissWaitingDialog();
            showOneToast(message);
        }
    }
}
