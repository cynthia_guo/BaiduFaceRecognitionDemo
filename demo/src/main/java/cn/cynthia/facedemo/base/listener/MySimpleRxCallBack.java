package cn.cynthia.facedemo.base.listener;

import cn.dlc.commonlibrary.okgo.rx.OkObserver;

/**
 * Created by guoshuifang on 2019/8/5.
 * TODO RX的回调
 */
public class MySimpleRxCallBack<T> extends OkObserver<T> {
    @Override
    public void onSuccess(T t) {

    }

    @Override
    public void onFailure(String message, Throwable tr) {

    }
}
