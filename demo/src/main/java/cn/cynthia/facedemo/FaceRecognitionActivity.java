package cn.cynthia.facedemo;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.TextureView;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.baidu.aip.FaceRecognitionCallback;
import com.baidu.aip.FaceSDKUtil;
import com.baidu.aip.face.TexturePreviewView;
import com.baidu.aip.widget.round.FaceRoundSDKUtil;
import com.google.gson.Gson;
import com.licheedev.myutils.LogPlus;
import com.trello.rxlifecycle2.android.ActivityEvent;

import butterknife.BindView;
import cn.cynthia.facedemo.base.BaseActivity;
import cn.cynthia.facedemo.bean.AddFaceBean;
import cn.cynthia.facedemo.bean.AssessToken;
import cn.cynthia.facedemo.bean.SearchBean;
import cn.cynthia.facedemo.utils.SpUtil;
import cn.dlc.commonlibrary.utils.ScreenUtil;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
//方形的预览效果
public class FaceRecognitionActivity extends BaseActivity {
    private static final String TAG = "FaceAddActivity";
    @BindView(R.id.texture_view)
    TextureView textureView;
    @BindView(R.id.preview_view)
    TexturePreviewView previewView;
    @BindView(R.id.fl_view)
    FrameLayout flView;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_face_recognition;
    }

    int type = 1;//1识别，2录入

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        type = getIntent().getIntExtra("type", 1);
        getToken();
        FaceRoundSDKUtil.setFaceRecognitionCallback(new FaceRecognitionCallback() {
            @Override
            public void onFace(final Bitmap info) {
                //进行人脸识别
                FaceRecognitionActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (type == 2) {
                            searchFace(info);
                        } else {
                            addFace(info);
                        }
                    }
                });
            }

            @Override
            public void onPress(final String tip) {
                FaceRecognitionActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(FaceRecognitionActivity.this, tip, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    private void getToken() {
        HttpManager.getInstance().getAssessToken()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(bindUntilEvent(ActivityEvent.DESTROY))
                .subscribe(new SimpleRxDataCallBack<AssessToken>("加载token中...", false) {
                    @Override
                    public void onSuccess(AssessToken searchBean) {
                        super.onSuccess(searchBean);
                        if (TextUtils.isEmpty(searchBean.error)) {
                            SpUtil.saveAssessToken(searchBean);
                            FaceSDKUtil.initFaceDetect(FaceRecognitionActivity.this
                                    , previewView, textureView, 2,
                                    ScreenUtil.getRealWidth(), ScreenUtil.getRealWidth());
                            FaceRoundSDKUtil.stop();
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    FaceRoundSDKUtil.resume();
                                }
                            }, 500);
                        } else {
                            onFailure(searchBean.error + "-" + searchBean.error_description, null);
                        }
                    }

                    @Override
                    public void onFailure(String message, Throwable tr) {
                        super.onFailure(message, tr);
                        LogPlus.d(message);
                    }
                });

    }

    //人脸校验识别中
    private void searchFace(Bitmap info) {
        if (info != null) {
            FaceRoundSDKUtil.stop();
            HttpManager.getInstance().searchFace(SystemUtil.bitmapToString(info), "BASE64")
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .compose(this.<SearchBean>bindUntilEvent(ActivityEvent.DESTROY))
                    .subscribe(new SimpleRxDataCallBack<SearchBean>("检测到人脸，识别中...", false) {
                        @Override
                        public void onSuccess(SearchBean searchBean) {
                            super.onSuccess(searchBean);
                            if (searchBean.error_code == 0 &&
                                    searchBean.result != null &&
                                    searchBean.result.user_list != null) {
                                for (SearchBean.ResultBean.UserListBean user : searchBean.result.user_list) {
                                    if (user.score >= 80) {
                                        //人脸识别通过....
                                        showOneToast("人脸识别成功");
                                        FaceRoundSDKUtil.setDetectBitmap(false);
                                        return;
                                    }
                                }
                            }
                            LogPlus.d("searbean==" + new Gson().toJson(searchBean));
                            onFailure("人脸识别失败", null);
                        }

                        @Override
                        public void onFailure(String message, Throwable tr) {
                            super.onFailure(message, tr);
                            FaceRoundSDKUtil.setDetectBitmap(false);
                            showOneToast(message);
                        }
                    });
        }
    }

    //增加人脸
    private void addFace(Bitmap info) {
        if (info != null) {
            FaceRoundSDKUtil.stop();
            HttpManager.getInstance().addFace(SystemUtil.bitmapToString(info), "BASE64")
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .compose(this.<AddFaceBean>bindUntilEvent(ActivityEvent.DESTROY))
                    .subscribe(new SimpleRxDataCallBack<AddFaceBean>("检测到人脸，新增中...", false) {
                        @Override
                        public void onSuccess(AddFaceBean searchBean) {
                            super.onSuccess(searchBean);
                            //人脸添加成功....

                            if (TextUtils.equals(searchBean.error_code, "0")) {
                                showOneToast("人脸添加成功");
                                FaceRoundSDKUtil.setDetectBitmap(false);
                                return;
                            }
                            onFailure("人脸添加失败---" + new Gson().toJson(searchBean), null);
                        }

                        @Override
                        public void onFailure(String message, Throwable tr) {
                            super.onFailure(message, tr);
                            Log.d(TAG, "人脸添加失败：msg=" + message);
                        }
                    });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        FaceRoundSDKUtil.resume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        FaceRoundSDKUtil.stop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        FaceRoundSDKUtil.setFaceRecognitionCallback(null);
        FaceRoundSDKUtil.destroy();
    }
}
