package cn.cynthia.facedemo;

/**
 * Created by guoshuifang on 2020/1/4.
 * TODO
 */
public interface Urls {
    String BaseUrl = "";
    String getAssessToken = "https://aip.baidubce.com/oauth/2.0/token";//获取百度token
    String addFace = "https://aip.baidubce.com/rest/2.0/face/v3/faceset/user/add";//添加人脸
    String searchFace = "https://aip.baidubce.com/rest/2.0/face/v3/search";//人脸识别
}
