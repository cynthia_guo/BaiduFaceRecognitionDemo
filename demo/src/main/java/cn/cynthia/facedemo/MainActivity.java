package cn.cynthia.facedemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    //人脸识别
    public void recognition(View v) {
        //方形预览效果
        startActivity(new Intent(this,FaceRecognitionActivity.class));
    }

    //人脸录入
    public void faceAdd(View view) {
        //圆形预览效果
        startActivity(new Intent(this,FaceAddActivity.class));
    }
}
