package cn.cynthia.facedemo;

import android.app.Application;
import android.content.Intent;

import com.baidu.aip.FaceSDKUtil;
import com.lzy.okgo.cookie.CookieJarImpl;
import com.lzy.okgo.cookie.store.SPCookieStore;

import cn.dlc.commonlibrary.okgo.OkGoWrapper;
import cn.dlc.commonlibrary.okgo.exception.ApiException;
import cn.dlc.commonlibrary.okgo.interceptor.ErrorInterceptor;
import cn.dlc.commonlibrary.okgo.logger.JsonRequestLogger;
import cn.dlc.commonlibrary.okgo.translator.DefaultErrorTranslator;
import cn.dlc.commonlibrary.utils.SystemUtil;
import cn.dlc.commonlibrary.utils.ToastUtil;
import okhttp3.OkHttpClient;

/**
 * Created by guoshuifang on 2020/1/4.
 * TODO
 */
public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        if (SystemUtil.isMainProcess(this)) {
            FaceSDKUtil.init(this, Information.LICENSE_ID, Information.LICENSE_FILE_NAME);
            initInternet();
        }
    }

    private void initInternet() {
        // 网络
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.cookieJar(new CookieJarImpl(new SPCookieStore(this)));
        OkGoWrapper.initOkGo(this, builder.build());
        OkGoWrapper.instance().setErrorTranslator(new DefaultErrorTranslator())
                // 拦截网络错误，一般是登录过期啥的
                .setErrorInterceptor(new ErrorInterceptor() {
                    @Override
                    public boolean interceptException(Throwable tr) {
                        if (tr instanceof ApiException) {
                            ApiException ex = (ApiException) tr;
                            if (ex.getCode() == 101) {
                                ToastUtil.showOne(getApplicationContext(), "登录信息已过期或已在其它设备登录,请重新登录!");
                                // 登录信息过期，请重新登录
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                intent.setFlags(
                                        Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                return true;
                            }
                        }
                        return false;
                    }
                })
                // 打印网络访问日志的
                .setRequestLogger(new JsonRequestLogger(BuildConfig.DEBUG, 30));
    }


}
